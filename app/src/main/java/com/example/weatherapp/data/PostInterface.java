package com.example.weatherapp.data;

import com.example.weatherapp.pojo.PostResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostInterface {


       @GET("/data/2.5/forecast?q=London,us&appid=439d4b804bc8187953eb36d2a8c26a02")
       Call<PostResponse> getPosts();
}
