package com.example.weatherapp.pojoc;

import com.example.weatherapp.pojo.City;
import com.example.weatherapp.pojo.Post;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    @SerializedName("city")
    private Cityc mCity;
    @SerializedName("cnt")
    private Long mCnt;
    @SerializedName("cod")
    private String mCod;
    @SerializedName("list")
    private List<Postsc> mList;
    @SerializedName("message")
    private Double mMessage;

    public Cityc getmCity() {
        return mCity;
    }

    public void setmCity(Cityc mCity) {
        this.mCity = mCity;
    }

    public Long getmCnt() {
        return mCnt;
    }

    public void setmCnt(Long mCnt) {
        this.mCnt = mCnt;
    }

    public String getmCod() {
        return mCod;
    }

    public void setmCod(String mCod) {
        this.mCod = mCod;
    }

    public List<Postsc> getmList() {
        return mList;
    }

    public void setmList(List<Postsc> mList) {
        this.mList = mList;
    }

    public Double getmMessage() {
        return mMessage;
    }

    public void setmMessage(Double mMessage) {
        this.mMessage = mMessage;
    }
}
