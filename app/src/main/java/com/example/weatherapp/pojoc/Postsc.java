package com.example.weatherapp.pojoc;

import com.example.weatherapp.pojo.City;
import com.example.weatherapp.pojoc.Mainc;
import com.example.weatherapp.pojoc.Weatherc;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Postsc {
    @SerializedName("main")
    private Mainc mMain;
    @SerializedName("weather")
    private List<Weatherc> mWeather;
    @SerializedName("city")
    private Cityc mCity;


    public Mainc getmMain() {
        return mMain;
    }

    public List<Weatherc> getmWeather() {
        return mWeather;
    }

    public Cityc getmCity() {
        return mCity;
    }

}
