package com.example.weatherapp.pojoc;

import com.google.gson.annotations.SerializedName;

public class Weatherc {
    @SerializedName("description")
    private String mDescription;

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

}
