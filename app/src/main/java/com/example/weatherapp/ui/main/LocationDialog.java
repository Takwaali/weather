package com.example.weatherapp.ui.main;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.weatherapp.R;
import com.example.weatherapp.pojo.PostResponse;
import com.example.weatherapp.pojoc.Cityc;
import com.example.weatherapp.pojoc.Postsc;
import com.example.weatherapp.pojoc.Response;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class LocationDialog extends AppCompatDialogFragment {
        private Spinner spinner;
    private ArrayList<Cityc> mCountryList;
    private CountryAdaapter mAdpter;
    private PostcViewmodel viewmodel;
        public Dialog onCreateDialog(Bundle savedInstanceState){

            AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
            LayoutInflater inflater=getActivity().getLayoutInflater();
            View view=inflater.inflate(R.layout.location_dialog,null);
            builder.setView(view).setTitle("location").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            spinner=view.findViewById(R.id.spinner);

            viewmodel = ViewModelProviders.of(this).get(PostcViewmodel.class);
            viewmodel.getPost();
            Log.d("takwa", "onChanged: response= "+new Gson());

            mAdpter=new CountryAdaapter(getContext(),mCountryList);
            Log.d("takwa", "onChanged: response= "+new Gson());
            spinner.setAdapter(mAdpter);
            Log.d("takwa", "onChanged: response= "+new Gson());
            viewmodel.postsMutableLiveData.observe(this, new Observer<Response>() {

                @Override

                public void onChanged(Response postcModels) {

                   mAdpter.setList((postcModels.getmList()) );
                    Log.d("takwa", "onResponse: body= "+new Gson().toJson(postcModels));
                }
            });
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Cityc countryitem= (Cityc) adapterView.getItemAtPosition(i);
                    if(countryitem !=null)
                    {
                        String country_name= countryitem.getmName();
                    Toast.makeText(getContext(),country_name+" selected",Toast.LENGTH_SHORT).show();}
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return builder.create();

        }

    private void initlist(){

        mCountryList =new ArrayList<>();
        mCountryList.add(new Cityc());
        mCountryList.add(new Cityc());


    }


}

