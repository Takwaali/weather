package com.example.weatherapp.ui.main;

import android.graphics.ColorSpace;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.R;
import com.example.weatherapp.pojo.Post;


import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PostViewHolder> {
    private List<Post> moviesList = new ArrayList<>();

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
int index=0;
        holder.description.setText(moviesList.get(position).getWeather().get(index).getDescription()+" ");
        holder.temp_max.setText(moviesList.get(position).getMain().getTempMax()+"   Max Temp");
        holder.temp_min.setText(moviesList.get(position).getMain().getTempMin()+"   Min Temp");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public void setList(List<Post> moviesList) {
        this.moviesList = moviesList;
        notifyDataSetChanged();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        TextView description, temp_max, temp_min;
        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.description);
            temp_max = itemView.findViewById(R.id.temp_max);

            temp_min = itemView.findViewById(R.id.tem_min);
        }
    }
}
