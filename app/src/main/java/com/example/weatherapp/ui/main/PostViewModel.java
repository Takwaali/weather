package com.example.weatherapp.ui.main;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.weatherapp.data.PostsClient;
import com.example.weatherapp.pojo.PostResponse;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostViewModel extends ViewModel
{
    MutableLiveData<PostResponse> postsMutableLiveData = new MutableLiveData<>();
    MutableLiveData<String> postsError = new MutableLiveData<>();


    public void getPosts()
    {
        PostsClient.getINSTANCE().getPosts().enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response)
            {
                PostResponse body = response.body();

                Log.d("takwa", "onResponse: code= "+response.code());
                Log.d("takwa", "onResponse: body= "+new Gson().toJson(body));


                if (response.isSuccessful() && body != null  && body.getCod().equals("200"))
                    postsMutableLiveData.setValue(body);
                else
                    postsError.setValue("Something went Wrong");
            }
            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                postsError.setValue("Something went Wrong");



                Log.e("takwa", "onFailure: error= ", t);
            }
        });
    }


}

