package com.example.weatherapp.ui.main;


import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.example.weatherapp.datac.CurrentClient;
import com.example.weatherapp.pojo.PostResponse;
import com.example.weatherapp.pojoc.Response;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;


public class PostcViewmodel extends ViewModel {

    MutableLiveData<Response> postsMutableLiveData = new MutableLiveData<>();
    MutableLiveData<String> postsError = new MutableLiveData<>();

    public void getPost(){
        CurrentClient.getINSTANCE().getPosts().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response body = response.body();
                Log.d("takwa", "onResponse: code= "+response.code());
                Log.d("takwa", "onResponse: body= "+new Gson().toJson(body));

                if (response.isSuccessful() && body != null  && body.getmCod().equals("200"))
                {
                    postsMutableLiveData.setValue(body);
                }
                else
                    postsError.setValue("Something went Wrong");
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                postsError.setValue("Something went Wrong");

                Log.e("takwa", "onFailure: error= ", t);
            }
        });

    }
}