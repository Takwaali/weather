package com.example.weatherapp.ui.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.weatherapp.R;

import com.example.weatherapp.pojo.Post;
import com.example.weatherapp.pojoc.Cityc;
import com.example.weatherapp.pojoc.Postsc;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class CountryAdaapter extends ArrayAdapter<Cityc> {
    private List<Postsc> moviesList = new ArrayList<>();
    public CountryAdaapter(@NonNull Context context, ArrayList<Cityc> countrylist) {
        super(context,0 ,countrylist);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initview(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initview(position, convertView, parent);
    }
    private View initview(int position, View convertView,  ViewGroup parent){
        if (convertView == null)
        {
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.country_spinner,parent,false);
        }

        TextView text_name=convertView.findViewById(R.id.country_name);

        Cityc currentitem=getItem(position);
        if (currentitem!=null)
        {
            text_name.setText(moviesList.get(position).getmCity().getmName()+" ");

        }
        return convertView;
    }

    public void setList(List<Postsc> moviesList) {
        this.moviesList = moviesList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (moviesList==null)
        {
            return 0;
        }
        else

        return moviesList.size();

    }

    @Nullable
    @Override
    public Cityc getItem(int position) {

        return moviesList.get(position).getmCity();
    }
}
