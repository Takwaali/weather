package com.example.weatherapp.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.example.weatherapp.R;

import com.example.weatherapp.ui.main.LocationDialog;

public class Setting extends AppCompatActivity {
TextView tv_location,location_map;
Switch bt_not;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        tv_location=findViewById(R.id.tv_location);
        tv_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              openDialog();
            }
        });

        bt_not=findViewById(R.id.sw_notification);

         creatNotificationchannel();

        bt_not.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bt_not.isChecked())
                {  NotificationCompat.Builder builder=new NotificationCompat.Builder(Setting.this,"My Notification");
                    builder.setContentTitle("Weather App");
                    builder.setContentText("Weather Today");
                    builder.setSmallIcon(R.drawable.ic_stat_name);
                    builder.setAutoCancel(true);
                    NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Setting.this);
                    managerCompat.notify(1,builder.build());
                    Intent intent=new Intent(Setting.this,Not.class);
                    PendingIntent pendingIntent=PendingIntent.getBroadcast(Setting.this,0,intent,0);
                    AlarmManager alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);

                    long timeclick= System.currentTimeMillis();
                    long tensecond =1000*10;
                    alarmManager.set(AlarmManager.RTC_WAKEUP,tensecond*timeclick,pendingIntent);

                }
                else {
                    NotificationCompat.Builder builder=new NotificationCompat.Builder(Setting.this,"My Notification");

                    builder.setAutoCancel(false);
                    NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Setting.this);
                    managerCompat.notify(0,builder.build());

                }

            }
        });
        location_map=findViewById(R.id.map_location);
        location_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent i =new Intent(Setting.this,Map.class);
             startActivity(i);
            }
        });

    }
    public void openDialog(){

LocationDialog locationDialog=new LocationDialog();

locationDialog.show(getSupportFragmentManager(),"location dialog");
    }



    private void creatNotificationchannel(){

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("My Notification", "My Notification", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);

        }
        }
}
