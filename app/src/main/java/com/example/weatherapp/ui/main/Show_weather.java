package com.example.weatherapp.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weatherapp.R;
import com.example.weatherapp.pojo.PostResponse;
import com.example.weatherapp.pojoc.Postsc;
import com.example.weatherapp.pojoc.Response;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Show_weather extends AppCompatActivity {
    PostViewModel postViewModel;
    PostcViewmodel postcViewmodel;
ImageView im_setting;
RecyclerView rec_showweather;
    SwipeRefreshLayout swipe;
TextView temp_max,temp_min,description,city;
    private PostcViewmodel viewmodel;
    private TextView tv_city;
    private TextView temp;
    private TextView max_temp;
    private TextView min_temp;
    private ProgressBar pb_loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_weather);
        temp_max=findViewById(R.id.temp);
        temp_min=findViewById(R.id.small_temp);

        description=findViewById(R.id.textView2);

        im_setting=findViewById(R.id.setting);
        swipe=findViewById(R.id.swipe);

        im_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I=new Intent(Show_weather.this, Setting.class);
                startActivity(I);
            }
        });


         rec();

        initViews();


        viewmodel = ViewModelProviders.of(this).get(PostcViewmodel.class);


        observeData();


        getPost();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               rec();
                initViews();

                observeData();


                getPost();

                swipe.setRefreshing(false);
            }
        });
    }
    private void rec()
    {

        postViewModel = ViewModelProviders.of(this).get(PostViewModel.class);

        postViewModel.getPosts();
        RecyclerView rec_showweather= findViewById(R.id.re_weather);
        final RecyclerAdapter adapter = new RecyclerAdapter();
        rec_showweather.setLayoutManager(new LinearLayoutManager(this));
        rec_showweather.setAdapter(adapter);

        postViewModel.postsMutableLiveData.observe(this, new Observer<PostResponse>() {
            @Override
            public void onChanged(PostResponse postModels) {

                adapter.setList(postModels.getList());
            }
        });
    }
    private void initViews()
    {
        viewmodel = ViewModelProviders.of(this).get(PostcViewmodel.class);
        tv_city = findViewById(R.id.date);
        temp = findViewById(R.id.textView2);
        max_temp = findViewById(R.id.temp);
        min_temp = findViewById(R.id.small_temp);
        pb_loading = findViewById(R.id.pb_loading);
    }


    private void getPost() {
        pb_loading.setVisibility(View.GONE);

        viewmodel.getPost();
    }

    private void observeData()
    {
        viewmodel.postsError.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {


                Toast.makeText(Show_weather.this, s, Toast.LENGTH_SHORT).show();
                pb_loading.setVisibility(View.GONE);
            }
        });

        viewmodel.postsMutableLiveData.observe(this, new Observer<Response>() {
            @Override
            public void onChanged(Response response) {


                pb_loading.setVisibility(View.GONE);

                Log.d("takwa", "onChanged: response= "+new Gson().toJson(response));

                Postsc postsc = response.getmList().get(0);
                tv_city.setText(response.getmCity().getmName());
                int index=0;
                temp.setText(""+postsc.getmWeather().get(index).getmDescription());
                max_temp.setText(""+postsc.getmMain().getTempMax());
                min_temp.setText(""+postsc.getmMain().getTempMin());
            }
        });


    }

}
