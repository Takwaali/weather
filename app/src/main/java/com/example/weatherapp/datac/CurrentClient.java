package com.example.weatherapp.datac;

import com.example.weatherapp.data.PostInterface;
import com.example.weatherapp.data.PostsClient;
import com.example.weatherapp.pojo.PostResponse;
import com.example.weatherapp.pojoc.Response;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CurrentClient {
    private static final String BASE_URL = "https://samples.openweathermap.org/";
    private Currentinterface currentinterface;
    private static CurrentClient INSTANCE;

    public CurrentClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        currentinterface = retrofit.create(Currentinterface.class);
    }

    public static CurrentClient getINSTANCE() {
        if (null == INSTANCE){
            INSTANCE = new CurrentClient();
        }
        return INSTANCE;
    }


    public Call<Response> getPosts(){
        return currentinterface.getPostc();
    }
}
