
package com.example.weatherapp.pojo;

import com.google.gson.annotations.SerializedName;

public class Sys {

    @SerializedName("pod")
    private String mPod;

    public String getPod() {
        return mPod;
    }

    public void setPod(String pod) {
        mPod = pod;
    }

}
