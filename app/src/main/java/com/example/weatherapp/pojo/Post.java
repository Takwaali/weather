
package com.example.weatherapp.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Post {
    @SerializedName("clouds")
    private Clouds mClouds;
    @SerializedName("dt")
    private Long mDt;
    @SerializedName("dt_txt")
    private String mDtTxt;
    @SerializedName("main")
    private Main mMain;
    @SerializedName("sys")
    private Sys mSys;
    @SerializedName("weather")
    private List<Weather> mWeather;
    @SerializedName("wind")
    private Wind mWind;
    @SerializedName("city")
    private City mCity;

    public City getCity() {
        return mCity;
    }

    public void setCity(City city) {
        mCity=city;
    }

    public Clouds getClouds() {
        return mClouds;
    }

    public void setClouds(Clouds clouds) {
        mClouds = clouds;
    }

    public Long getDt() {
        return mDt;
    }

    public void setDt(Long dt) {
        mDt = dt;
    }

    public String getDtTxt() {
        return mDtTxt;
    }

    public void setDtTxt(String dtTxt) {
        mDtTxt = dtTxt;
    }

    public Main getMain() {
        return mMain;
    }

    public void setMain(Main main) {
        mMain = main;
    }

    public Sys getSys() {
        return mSys;
    }

    public void setSys(Sys sys) {
        mSys = sys;
    }

    public List<Weather> getWeather() {
        return mWeather;
    }

    public void setWeather(List<Weather> weather) {
        mWeather = weather;
    }

    public Wind getWind() {
        return mWind;
    }

    public void setWind(Wind wind) {
        mWind = wind;
    }


}
